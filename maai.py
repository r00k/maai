#!/usr/bin/env python3

import nmap
import os
import argparse
import time
import requests

def init_scan(target, udp):
    '''
    fast full port scan of target
    parameters:
        target: string, IP or CIDR range of targets
        udp: bool, whether or not UDP scanning is enabled
    returns: dict
        scan_result: dict, default output of python-name scan
    '''
    nm = nmap.PortScanner()
    if udp:
        scan_result = nm.scan(target, 'T:0-65535,U:0-65535', arguments='-sS -sU -T4 --max-retries 0')
    else:
        scan_result = nm.scan(target, '0-65535', arguments='-Pn -T4 --max-retries 0')
    return scan_result

def init_scan_parse(init_scan_results):
    '''
    parses fast all ports scan of target returns open ports
    parameters:
        init_scan_results: dict, standard python-nmap output dict
    return: list
        host_dicts: list of {host, port, proto} dicts
    '''
    host_dicts = []
    for host in init_scan_results["scan"]:
        for proto in init_scan_results["scan"][host]:
            if proto != "tcp" and proto != "udp":
                continue
            port_list = []
            for port in init_scan_results["scan"][host][proto]:
                if "open" in init_scan_results["scan"][host][proto][port]["state"]:
                    port_list.append(str(port))
            host_dicts.append({"host":host, "ports":port_list, "proto":proto})
    return host_dicts

def script_scan(host, port_list, proto):
    '''
    initiate nse script scan against only open ports on target host
    parameters:
        host: string, target IP
        port_list: list, ports to scan
        proto: string, tcp or udp
    returns: list
        host: string, target IP
        proto: string, tcp or udp
        script_results: dict, contents of detailed scan
    '''
    nm = nmap.PortScanner()
    ports = ",".join(port_list)
    try:
        os.makedirs('./{}'.format(host))
    except:
        pass
    if proto == 'udp':
        script_results = nm.scan(hosts=host, ports=ports, arguments='-sU -A -oN {}/{}-scripts.nmap'.format(host, proto))
    elif proto == 'tcp':
        script_results = nm.scan(hosts=host, ports=ports, arguments='-Pn -A -oN {}/{}-scripts.nmap'.format(host, proto))
    print("[+] nmap results written to ./{}/{}-scripts.nmap".format(host, proto))
    return [host, proto, script_results]

def parse_web_servers(host, proto, script_dict):
    '''
    parses script scan results and finds http speaking ports
    parameters:
        host: string, host IP of target
        proto: string, tcp or udp
        script_dict: dict, actual script scan results
    returns: list
        host: string, host IP of target
        proto: string, tcp or udp
        web_ports: list, ports speaking http
    '''        
    web_ports = []
    for port in script_dict["scan"][host][proto]:
        if "http" in script_dict["scan"][host][proto][port]["name"] or \
           "http" in script_dict["scan"][host][proto][port]["product"]:
            web_ports.append(port)
    if len(web_ports) > 0:
        return [host, proto, web_ports]
    else:
        return None

def gen_dirsearch_command(host, proto, port):
    '''
    tries to determine underlying tech (php, flask, etc) and generate appropriate dirsearch command list
    parameters:
        host: string, IP of target host
        proto: string, tcp or udp. Not used right now, built for future proofing UDP http style targets
        port: string, web port
    returns: None
    '''
    passive_ext_detection = False # variable to track if we passively detected extension from redirected URL
    ext_found = []
    possible_index_ext = ["html", "php", "asp", "aspx", "htm", "pl", "cgi", "cfm", "cfml", \
                          "php3", "php4", "php5", "phtml"]
    address = "{}:{}".format(host, port)
    # determine cannonical address by following redirects
    initial_url_check = requests.get("http://{}/".format(address))
    if initial_url_check.status_code == 200:
        address = initial_url_check.url
    else:
        print("web server acting irregularly, fingerprint manually.")
        return
    # passively detect base technology
    for ext in possible_index_ext:
        if address[-len(ext):] == ext:
            ext_found.append(ext)
            passive_ext_detection = True
            break
    # actively detect base technology
    if not passive_ext_detection:
        for ext in possible_index_ext:
            active_detection_request = requests.get("{}index.{}".format(address, ext))
            if active_detection_request.status_code == 200:
                ext_found.append(ext)
                break
    if len(ext_found) > 0:
        print("[+] Extensions found: {}, use the following dirsearch command for recon".format(len(ext_found)))
        print("[+] dirsearch -u {} -e {},txt,bak,/ -f -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt \
-t 30 --ua='Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 \
Firefox/63.0'".format(address, ",".join(ext_found)))
    else:
        print("[!] No extra extensions found. You can try dirsearch with the following command:")
        print("[+] dirsearch -u {} -e txt,bak,/ -f -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt \
-t 30 --ua='Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 \
Firefox/63.0'".format(address))

def main():
    start_time = int(time.time())
    script_dicts = [] # list to store each (host, proto, {script scan results})
    web_servers = [] # list to store (host, proto, [web ports])
    options = argparse.ArgumentParser()
    options.add_argument("target", type=str, help="The IP or CIDR notation of the scanning target")
    options.add_argument("-u", "--udp", action="store_true", help="Adds udp to scan profile")
    options.add_argument("-v", "--verbose", action="store_true", help="Prints out debugging information")
    args = options.parse_args()
    print("[+] +{} - Beginning initial full port scan against targets".format(str(start_time - start_time)))
    init_results = init_scan(args.target, args.udp)
    print("[+] +{} - Initial full port scan complete. Parsing results...".format(str(int(time.time()) - start_time)))
    host_dicts = init_scan_parse(init_results)
    print("[+] +{} - Initial port scan results parsed.".format(str(int(time.time()) - start_time)))
    for host in host_dicts:
        print("[+] +{} - Beginning targeted script scans for host {}".format(str(int(time.time()) - start_time), \
                                                                             host["host"]))
        script_dicts.append(script_scan(host["host"], host["ports"], host["proto"]))
    print("[+] +{} - Targeted script scans complete for all hosts".format(str(int(time.time()) - start_time)))
    for entry in script_dicts:
        print("[+] +{} - Parsing {} scan for http servers".format(str(int(time.time()) - start_time), entry[0]))
        web_results = parse_web_servers(entry[0], entry[1], entry[2]) # entry = [host, proto, scan dict]
        if web_results != None:
            print("[+] +{} - Web ports found on host {}".format(str(int(time.time()) - start_time), entry[0]))
            web_servers.append(web_results) # web_results = [host, proto, list of web ports]
    for web_server in web_servers:
        for port in web_server[2]:
            gen_dirsearch_command(web_server[0], web_server[1], port) # web_server = [host, proto, web port]
    

if __name__ == '__main__':
    main()