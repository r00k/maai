# maai

Scanner that uses the python libraries for nmap to run full port scan on targets, then runs nse scans on only found open ports.

```
usage: maai [-h] [-u] [-v] target

positional arguments:
  target         The IP or CIDR notation of the scanning target

optional arguments:
  -h, --help     show this help message and exit
  -u, --udp      Adds udp to scan profile
  -v, --verbose  Prints out debugging information
```

todo:

* Extend web tech detection to follow non standard redirects (js based, etc)
* Enable advanced web tech detection (currently only finds index.* extensions)
* Add more flexibility for other scan types